<?php
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
?>

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-cubes mr-1"></i>
                        Seleccionar tema
                    </h3>                
                </div>
                <form action="./?controller=<?php echo $_GET['controller']; ?>&action=<?php echo $_GET['action']; ?>" method="post">
                    <div class="card-body">
                        <div class="row">                            
                            <!-- select -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tema a elegir:</label>
                                    <select class="form-control" name="tema" id="tema">
                                        <option value="dark-mode">Tema oscuro</option>                                        
                                        <option value="" <?php echo (!isset($_COOKIE['tema']) || $_COOKIE['tema'] != 'dark-mode') ? 'selected' : '';?>>Tema claro</option>                                        
                                    </select>
                                    <?php
                                    if(isset($errors['id_padre'])){
                                    ?>
                                    <p class="text-danger"><small><?php echo $errors['id_padre']; ?></small></p>
                                    <?php
                                    }
                                    ?>
                                </div>   
                            </div>
                        </div></div>
                    <div class="card-footer">                                                
                        <button type="submit" name="action" class="btn btn-primary mr-3 float-right" value="guardar">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



