<?php
namespace Com\Daw2\Controllers;

class UsuarioSistemaController extends \Com\Daw2\Core\BaseController
{
   
   public function index ()
   {      
       //TODO listado de usuarios sistema
        $_vars = array('titulo' => 'Panel de control',
                      'breadcumb' => array('Inicio' => array('url' => '#', 'active' => false)));
        $this->view->showViews(array('templates/header.view.php', '', 'templates/footer.view.php'), $_vars);      
   }
   
   public function testInsert(){
       $rol = new \Com\Daw2\Helpers\Rol(1, 'Administrador', '', '');
       for($i = 0; $i < 10; $i++){
            $email = substr(md5(random_int(1, PHP_INT_MAX)), 0 , 15).'@test.org';
            $usuario = new \Com\Daw2\Helpers\UsuarioSistema(NULL, $rol, $email, 'Test', 'es', false);
            $usuarioSistemaModel = new \Com\Daw2\Models\UsuarioSistemaModel();
            $usuarioSistemaModel->insertUsuarioSistema($usuario, 'test');
       }
   }
   
   public function testLogin(){
       $usuarioSistemaModel = new \Com\Daw2\Models\UsuarioSistemaModel();
       var_dump($usuarioSistemaModel->login('admin@test.org', 'test'));
   }
   
   
}