<?php

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

namespace Com\Daw2\Controllers;

/**
 * Description of CookiesController
 *
 * @author Rafael González Centeno
 */
class SessionController extends \Com\Daw2\Core\BaseController {
    
    public function sessionForm(){        
        $_vars = array('titulo' => 'Form Session');
        if(isset($_POST['action'])){
            $_errors = [];
            if(strlen($_POST['nombre'])){
                /* Si viene JS o HTML lo guardamos tal cual pero debemos tenerlo 
                 * en cuenta al mostrarlo. Si queremos también podríamos guardarlo ya saneado.*/
                $_SESSION['usuario'] = $_POST['nombre'];                                 
            }
            else{
                $_errors['nombre'] = 'Inserte un nombre';
            }
            $_vars['errors'] = $_errors;
        }        
        $this->view->showViews(array('templates/header.view.php', 'session_form.view.php', 'templates/footer.view.php'), $_vars);     
    }
    
    public function borrarVariableSession(){
        unset($_SESSION['test']); //Borramos sólo una variable
        //session_unset(); //Borra todas la variables
        //session_destroy();//Destruye la sesión
        
        $this->view->showViews(array('templates/header.view.php', 'session.view.php', 'templates/footer.view.php'), array('titulo' => 'Borrar Cookies'));     
    }
}
