<?php

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

namespace Com\Daw2\Controllers;

/**
 * Description of CookiesController
 *
 * @author Rafael González Centeno
 */
class CookiesController extends \Com\Daw2\Core\BaseController {
    
    public function testCookie(){
        setcookie('test', '<b>Este es un valor nuevo</b>'); //Cuidado porque el código HTML o script se mostraría tal cual. Es necesaria escapar la salida.
        setrawcookie('testraw2', 'dark-mode');
        //setrawcookie('testraw', '<b>Este es un valor raw</b>'); //Da error porque no puede tener caracteres especiales
        
        $this->view->showViews(array('templates/header.view.php', 'cookies.view.php', 'templates/footer.view.php'), array('titulo' => 'Test Cookies'));     
    }
    
    public function borrarCookie(){
        if(isset($_COOKIE['test'])){
            unset($_COOKIE['test']);
            setcookie('test', '', time()-3600);
        }
        if(isset($_COOKIE['testraw'])){
            unset($_COOKIE['testraw']);
            setcookie('testraw', '', time()-3600);
        }
        if(isset($_COOKIE['testraw2'])){
            unset($_COOKIE['testraw2']);
            setcookie('testraw2', '', time()-3600);
        }
        $this->view->showViews(array('templates/header.view.php', 'cookies.view.php', 'templates/footer.view.php'), array('titulo' => 'Borrar Cookies'));     
    }
    
    public function editTema(){
        if(isset($_POST['action'])){
            setcookie('tema', $_POST['tema'], time()+60*60*24*30);
        }
        $this->view->showViews(array('templates/header.view.php', 'tema.edit.view.php', 'templates/footer.view.php'), array('titulo' => 'Editar tema'));     
    }
}
